-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2019 at 06:26 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portalpro`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id` int(11) NOT NULL,
  `nim` int(10) NOT NULL,
  `modul` int(11) NOT NULL,
  `token` varchar(5) NOT NULL,
  `kode_asisten` varchar(3) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id`, `nim`, `modul`, `token`, `kode_asisten`, `status`, `created_at`, `updated_at`) VALUES
(6, 1202180000, 1, 'Y5UBO', 'ERZ', 'ABSVR', '2019-08-18 22:25:41', '2019-08-18 22:25:41'),
(7, 1202180000, 2, 'AAAAA', 'ERZ', 'ABSIN', '2019-08-18 22:25:41', '2019-08-18 22:25:41'),
(8, 1202180001, 2, 'BBBBB', 'ERZ', 'ABSVR', '2019-08-18 22:25:41', '2019-08-18 22:25:41');

-- --------------------------------------------------------

--
-- Table structure for table `detail_praktikum`
--

CREATE TABLE `detail_praktikum` (
  `id` int(11) NOT NULL,
  `id_praktikum` int(11) NOT NULL,
  `komponen_nilai` varchar(255) NOT NULL,
  `persentase_nilai` int(3) NOT NULL,
  `jumlah_soal` int(2) NOT NULL,
  `durasi` int(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_praktikum`
--

INSERT INTO `detail_praktikum` (`id`, `id_praktikum`, `komponen_nilai`, `persentase_nilai`, `jumlah_soal`, `durasi`, `created_at`, `updated_at`) VALUES
(23, 42, 'PV1TA', 15, 15, 15, '2019-08-10 14:57:54', '2019-08-10 14:57:54'),
(24, 42, 'PV2JN', 50, 3, 60, '2019-08-10 14:57:54', '2019-08-10 14:57:54'),
(25, 42, 'PV3AD', 35, 15, 20, '2019-08-10 14:57:54', '2019-08-10 14:57:54');

-- --------------------------------------------------------

--
-- Table structure for table `detail_user_praktikum`
--

CREATE TABLE `detail_user_praktikum` (
  `id` int(11) NOT NULL,
  `id_praktikum` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `kode_asisten` varchar(3) NOT NULL,
  `role` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_user_praktikum`
--

INSERT INTO `detail_user_praktikum` (`id`, `id_praktikum`, `nim`, `kode_asisten`, `role`, `status`, `created_at`, `updated_at`) VALUES
(7, 42, 1202174083, 'ERZ', 'ROLAS', 'ATSAC', '2019-08-10 15:05:30', '2019-08-10 15:05:30'),
(13, 42, 1202180000, '', 'ROLPR', 'ATSAC', '2019-08-17 00:00:00', '2019-08-17 00:00:00'),
(14, 42, 1202180002, '', 'ROLPR', 'ATSAC', '2019-08-17 00:00:00', '2019-08-17 00:00:00');

--
-- Triggers `detail_user_praktikum`
--
DELIMITER $$
CREATE TRIGGER `user_praktikum_aktif` AFTER INSERT ON `detail_user_praktikum` FOR EACH ROW UPDATE users
 SET praktikum_aktif = NEW.id_praktikum
 WHERE
 nim = NEW.nim
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `id_praktikum` int(11) NOT NULL,
  `kode_asisten` varchar(3) NOT NULL,
  `feedback` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mastercode`
--

CREATE TABLE `mastercode` (
  `id` int(11) NOT NULL,
  `prefix` varchar(3) NOT NULL,
  `kode_induk` varchar(5) DEFAULT NULL,
  `kode` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mastercode`
--

INSERT INTO `mastercode` (`id`, `prefix`, `kode_induk`, `kode`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'PTM', '', 'PTMCL', 'Class Lab', '2019-08-10 10:43:49', '2019-08-10 10:43:49'),
(2, 'PTM', '', 'PTMMT', 'Mentoring', '2019-08-10 10:43:49', '2019-08-10 10:43:49'),
(3, 'ROL', '', 'ROLAD', 'Admin', '2019-08-10 10:43:49', '2019-08-10 10:43:49'),
(4, 'ROL', '', 'ROLAS', 'Asisten', '2019-08-10 10:43:49', '2019-08-10 10:43:49'),
(5, 'ROL', '', 'ROLPR', 'Praktikan', '2019-08-10 10:43:50', '2019-08-10 10:43:50'),
(6, 'ATS', '', 'ATSAC', 'Aktif', '2019-08-10 10:43:50', '2019-08-10 10:43:50'),
(7, 'ATS', '', 'ATSNA', 'Non Aktif', '2019-08-10 10:43:50', '2019-08-10 10:43:50'),
(8, 'PV1', 'PVC', 'PV1TA', 'Tes Awal', '2019-08-10 10:43:50', '2019-08-10 10:43:50'),
(9, 'PV1', 'PVC', 'PV1TP', 'Tugas Pendahuluan', '2019-08-10 10:43:50', '2019-08-10 10:43:50'),
(10, 'PV2', 'PVC', 'PV2JN', 'Jurnal', '2019-08-10 10:43:50', '2019-08-10 10:43:50'),
(11, 'PV3', 'PVC', 'PV3AD', 'Audit', '2019-08-10 10:43:50', '2019-08-10 10:43:50'),
(12, 'PV3', 'PVC', 'PV3TR', 'Tes Akhir', '2019-08-10 10:43:50', '2019-08-10 10:43:50'),
(13, 'PV3', 'PVC', 'PV3WA', 'Weekly Assignment', '2019-08-10 10:43:50', '2019-08-10 10:43:50');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modul`
--

CREATE TABLE `modul` (
  `id` int(11) NOT NULL,
  `id_praktikum` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kj_tesawal1` varchar(255) DEFAULT NULL,
  `kj_tesawal2` varchar(255) DEFAULT NULL,
  `kj_tesakhir1` varchar(255) DEFAULT NULL,
  `kj_tesakhir2` varchar(255) DEFAULT NULL,
  `link_modul` varchar(255) DEFAULT NULL,
  `link_video` varchar(255) DEFAULT NULL,
  `link_tesawal1` varchar(255) DEFAULT NULL,
  `link_tesawal2` varchar(255) DEFAULT NULL,
  `link_jurnal1` varchar(255) DEFAULT NULL,
  `link_jurnal2` varchar(255) DEFAULT NULL,
  `link_tesakhir1` varchar(255) DEFAULT NULL,
  `link_tesakhir2` varchar(255) DEFAULT NULL,
  `durasi_input_nilai` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modul`
--

INSERT INTO `modul` (`id`, `id_praktikum`, `nama`, `kj_tesawal1`, `kj_tesawal2`, `kj_tesakhir1`, `kj_tesakhir2`, `link_modul`, `link_video`, `link_tesawal1`, `link_tesawal2`, `link_jurnal1`, `link_jurnal2`, `link_tesakhir1`, `link_tesakhir2`, `durasi_input_nilai`, `created_at`, `updated_at`) VALUES
(1, 42, 'Search Algorithm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, '2019-08-16 00:00:00', '2019-08-16 00:00:00'),
(2, 42, 'Stack & Queue', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, '2019-08-16 00:00:00', '2019-08-16 00:00:00'),
(3, 42, 'Linked List', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, '2019-08-16 00:00:00', '2019-08-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id` int(11) NOT NULL,
  `id_praktikum` int(11) NOT NULL,
  `id_modul` int(11) NOT NULL,
  `nim` int(255) NOT NULL,
  `kode_asisten_tesawal` varchar(3) DEFAULT NULL,
  `jawaban_tesawal` varchar(255) DEFAULT NULL,
  `tes_awal` double DEFAULT NULL,
  `kode_asisten_praktikum` varchar(3) DEFAULT NULL,
  `jurnal` double DEFAULT NULL,
  `jawaban_tesakhir` varchar(255) DEFAULT NULL,
  `tes_akhir` double DEFAULT NULL,
  `nilai_akhir` double DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id`, `id_praktikum`, `id_modul`, `nim`, `kode_asisten_tesawal`, `jawaban_tesawal`, `tes_awal`, `kode_asisten_praktikum`, `jurnal`, `jawaban_tesakhir`, `tes_akhir`, `nilai_akhir`, `created_at`, `updated_at`) VALUES
(23, 42, 1, 1202180000, 'ERZ', NULL, 45, 'ERZ', NULL, NULL, NULL, 9, '2019-08-26 23:14:27', '2019-08-26 23:14:27'),
(24, 42, 1, 1202180002, 'ERZ', NULL, 87.5, 'ERZ', 100, NULL, 100, 97.5, '2019-08-26 23:14:27', '2019-08-26 23:14:27'),
(25, 42, 2, 1202180002, 'ERZ', NULL, 76, 'ERZ', 90, NULL, 64.7, 82.1, '2019-08-26 23:22:24', '2019-08-26 23:22:24');

-- --------------------------------------------------------

--
-- Table structure for table `praktikum`
--

CREATE TABLE `praktikum` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tahun` int(4) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'ATSAC',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `praktikum`
--

INSERT INTO `praktikum` (`id`, `nama`, `tahun`, `tipe`, `status`, `created_at`, `updated_at`) VALUES
(42, 'Struktur dan Algoritma', 2020, 'PTMCL', 'ATSAC', '2019-08-10 14:57:54', '2019-08-10 15:03:22');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `expired_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`id`, `token`, `created_at`, `expired_at`) VALUES
(1, 'K5TKU', '2019-08-15 16:04:21', '2019-08-15 16:14:21'),
(2, 'ORPUE', '2019-08-15 16:10:15', '2019-08-15 16:20:15'),
(3, '4MSE9', '2019-08-15 16:11:10', '2019-08-15 16:21:10'),
(4, 'XSLFT', '2019-08-15 16:11:16', '2019-08-15 16:21:16'),
(5, 'TVZ6M', '2019-08-15 16:12:11', '2019-08-15 16:22:11'),
(6, 'S0BXT', '2019-08-15 16:12:53', '2019-08-15 16:22:53'),
(7, 'ACWEY', '2019-08-15 16:13:01', '2019-08-15 16:23:01'),
(8, 'YRFB9', '2019-08-15 16:13:18', '2019-08-15 16:23:18'),
(9, 'KI1JM', '2019-08-15 16:13:35', '2019-08-15 16:23:35'),
(10, 'NGTLF', '2019-08-15 16:13:46', '2019-08-15 16:23:46'),
(11, 'RASQB', '2019-08-15 16:14:47', '2019-08-15 16:24:47'),
(12, 'NZWF2', '2019-08-15 16:15:17', '2019-08-15 16:25:17'),
(13, 'IVPIH', '2019-08-15 16:18:08', '2019-08-15 16:28:08'),
(14, '5JYHW', '2019-08-15 16:18:16', '2019-08-15 16:28:16'),
(15, 'DNVLK', '2019-08-16 11:46:24', '2019-08-16 11:56:24'),
(16, '03KDY', '2019-08-17 13:41:50', '2019-08-17 13:51:50'),
(17, 'SFYZW', '2019-08-17 16:27:52', '2019-08-17 16:37:52'),
(18, 'Y5UBO', '2019-08-18 19:10:54', '2019-08-18 19:20:54'),
(19, 'XIQMP', '2019-08-24 22:59:48', '2019-08-24 23:09:48');

-- --------------------------------------------------------

--
-- Table structure for table `tp`
--

CREATE TABLE `tp` (
  `id` int(11) NOT NULL,
  `id_praktikum` int(11) NOT NULL,
  `id_modul` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `kode_asisten` varchar(3) NOT NULL,
  `file` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tp`
--

INSERT INTO `tp` (`id`, `id_praktikum`, `id_modul`, `nim`, `kode_asisten`, `file`, `created_at`, `updated_at`) VALUES
(17, 42, 1, 1202180002, 'ERZ', 'ERZ_UMI.rar', '2019-08-25 16:58:12', '2019-08-25 16:58:12'),
(18, 42, 2, 1202180002, 'ERZ', 'ERZ_UMI_MODUL2.rar', '2019-08-25 16:58:20', '2019-08-25 16:58:20'),
(19, 42, 1, 1202180000, 'ERZ', 'TP-Copy.rar', '2019-08-26 07:08:53', '2019-08-26 07:08:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `kode_asisten` varchar(3) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `praktikum_aktif` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nim`, `nama`, `kelas`, `password`, `kode_asisten`, `role`, `praktikum_aktif`, `created_at`, `updated_at`) VALUES
(1, 1202174082, 'Umi Zahroh', 'SI4101', '$2y$10$tanLIRc.xQ81s8iEBJtr9etgYHHO5kKfiMUXsgsn.NkJz6/k9UmPS', 'ZAH', 'ROLAD', NULL, '2019-08-09 22:13:57', '2019-08-09 17:13:37'),
(2, 1202180000, 'Maemunah', 'SI4200', '$2y$10$tanLIRc.xQ81s8iEBJtr9etgYHHO5kKfiMUXsgsn.NkJz6/k9UmPS', NULL, 'ROLPR', 42, '2019-08-10 01:38:32', '2019-08-10 01:38:36'),
(3, 1202174083, 'Erza Putra Albasori', 'SI4107', '$2y$10$tanLIRc.xQ81s8iEBJtr9etgYHHO5kKfiMUXsgsn.NkJz6/k9UmPS', 'ERZ', 'ROLAS', NULL, '2019-08-09 22:13:57', '2019-08-09 17:13:37'),
(4, 1202180001, 'Maemunah 2', 'SI4200', '$2y$10$tanLIRc.xQ81s8iEBJtr9etgYHHO5kKfiMUXsgsn.NkJz6/k9UmPS', NULL, 'ROLPR', 42, '2019-08-10 01:38:32', '2019-08-10 01:38:36'),
(5, 1202180002, 'Budiman', 'SI4200', '$2y$10$ozsk0IOoGxv/Cw8mJsd4N.TzG9paF7WLCanQi/gWxTGi7RJk6.CD.', 'ERZ', 'ROLPR', 42, '2019-08-25 00:00:00', '2019-08-25 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_praktikum`
--
ALTER TABLE `detail_praktikum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_praktikum_fk0` (`id_praktikum`);

--
-- Indexes for table `detail_user_praktikum`
--
ALTER TABLE `detail_user_praktikum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_user_praktikum_fk0` (`id_praktikum`),
  ADD KEY `detail_user_praktikum_fk1` (`nim`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mastercode`
--
ALTER TABLE `mastercode`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modul`
--
ALTER TABLE `modul`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modul_fk0` (`id_praktikum`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nilai_fk0` (`id_praktikum`),
  ADD KEY `nilai_fk1` (`id_modul`);

--
-- Indexes for table `praktikum`
--
ALTER TABLE `praktikum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tp`
--
ALTER TABLE `tp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nim` (`nim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `detail_praktikum`
--
ALTER TABLE `detail_praktikum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `detail_user_praktikum`
--
ALTER TABLE `detail_user_praktikum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mastercode`
--
ALTER TABLE `mastercode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modul`
--
ALTER TABLE `modul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `praktikum`
--
ALTER TABLE `praktikum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tp`
--
ALTER TABLE `tp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_praktikum`
--
ALTER TABLE `detail_praktikum`
  ADD CONSTRAINT `detail_praktikum_fk0` FOREIGN KEY (`id_praktikum`) REFERENCES `praktikum` (`id`);

--
-- Constraints for table `detail_user_praktikum`
--
ALTER TABLE `detail_user_praktikum`
  ADD CONSTRAINT `detail_user_praktikum_fk0` FOREIGN KEY (`id_praktikum`) REFERENCES `praktikum` (`id`),
  ADD CONSTRAINT `detail_user_praktikum_fk1` FOREIGN KEY (`nim`) REFERENCES `users` (`nim`);

--
-- Constraints for table `modul`
--
ALTER TABLE `modul`
  ADD CONSTRAINT `modul_fk0` FOREIGN KEY (`id_praktikum`) REFERENCES `praktikum` (`id`);

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `nilai_fk0` FOREIGN KEY (`id_praktikum`) REFERENCES `praktikum` (`id`),
  ADD CONSTRAINT `nilai_fk1` FOREIGN KEY (`id_modul`) REFERENCES `modul` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
