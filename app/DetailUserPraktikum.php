<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailUserPraktikum extends Model
{
    protected $table = "detail_user_praktikum";

    public function users()
    {
        return $this->hasOne('App\User', 'nim', 'nim');
    }

}
