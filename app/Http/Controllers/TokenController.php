<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Token;

class TokenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(){
        return redirect('/token');
    }

    public function generate()
    {
    	date_default_timezone_set('Asia/Jakarta');
    	$currentDate = strtotime(date("Y-m-d H:i:s"));
    	$interval = 10;
    	$expired_at = date("Y-m-d H:i:s", $currentDate + (60 * $interval));
		$randomString = strtoupper(Str::random(5));

		$data = [
			'token' => $randomString,
			'created_at' => date("Y-m-d H:i:s"),
			'expired_at' => $expired_at
		];

		$save = DB::table('token')->insert($data);

		return view('generatetoken', $data);
    }

}
