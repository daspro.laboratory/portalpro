<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class NilaiController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		date_default_timezone_set('Asia/Jakarta');
        $date = date("Y-m-d H:i:s");

		$nim = Auth::user()->nim;
		$data['result'] = DB::table('nilai')
		->join('modul', 'modul.id', '=', 'nilai.id_modul')
		->select('nilai.*')
		->where([['nilai.nim', $nim], ['modul.publish_date', '<=', $date]])
		->get();

		return view('nilai', $data);
	}
}
