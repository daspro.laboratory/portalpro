<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use Storage;
use Response;
Use File;

class InputTPController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		DB::enableQueryLog();
	}

	public function index()
	{   
		$user_praktikum = Auth::user()->praktikum_aktif;
		$user_nim = Auth::user()->nim;

		$data['assistant'] = DB::select(
			DB::raw("SELECT id, nim, kode_asisten FROM detail_user_praktikum
				WHERE id_praktikum = '". $user_praktikum ."'
				AND role = 'ROLAS'")
		);

		$data['modul'] = DB::select(
			DB::raw("SELECT modul.id, modul.nama FROM modul
				WHERE modul.id_praktikum = '". $user_praktikum ."'
				AND NOT EXISTS (SELECT * FROM tp
				WHERE tp.id_modul = modul.id
				AND tp.nim = '". $user_nim ."')")
		);

		$data['modul'] = DB::select(
			DB::raw("SELECT modul.id, modul.nama FROM modul
				WHERE modul.id_praktikum = '". $user_praktikum ."'
				AND NOT EXISTS (SELECT * FROM tp
				WHERE tp.id_modul = modul.id
				AND tp.nim = '". $user_nim ."')")
		);

		$data['history'] = DB::select(
			DB::raw("SELECT id_modul, kode_asisten, file
				FROM tp
				WHERE nim = '". $user_nim ."'")
		);

		return view('inputtp', $data);
	}

	public function upload(Request $request)
	{
		Validator::make($request->all(), [
			'file' => 'required',
			'assistant' => 'required',
			'modul' => 'required'
		])->validate();

		$file = $request->file('file');
		$file_name = $file->getClientOriginalName();
		$file->move(storage_path('app/public/tp'), $file_name);

		$date = date("Y-m-d H:i:s");
		$params = [
			'id_praktikum' => Auth::user()->praktikum_aktif,
			'id_modul' => $request->modul,
			'nim' => Auth::user()->nim,
			'kode_asisten' => $request->assistant,
			'file' => $file_name,
			'created_at' => $date,
			'updated_at' => $date
		];

		$save = DB::table('tp')->insert($params);
		echo json_encode($save);
	}

	public function download()
	{
		$user_praktikum = Auth::user()->praktikum_aktif;
		$user_nim = Auth::user()->nim;
		$user_code = Auth::user()->kode_asisten;

		$data['modul'] = DB::select(
            DB::raw("SELECT modul.id, modul.nama FROM modul
                      WHERE EXISTS (SELECT * FROM tp
                                         	WHERE tp.id_modul = modul.id)")
        );

		$data['tp_1'] = [];
        $data['tp'] = [];
        foreach ($data['modul'] as $row) {
        	$data['tp'][$row->id] = DB::select(
        		DB::raw("SELECT tp.nim, users.nama, users.kelas, tp.file, DATE_FORMAT(tp.created_at, '%d %M %Y %T') AS created_at
        			 	   FROM tp, users
        			 	  WHERE tp.id_modul = '". $row->id ."'
        			 	    AND tp.nim = users.nim
        			 	    AND tp.kode_asisten = '" . $user_code . "'")
        	);

        }

        if (!empty($data['tp'])) {
        	$data['tp_1'] = $data['tp'][1];	
        }
        
        $data['tp'] = json_encode($data['tp']);
		
		return view('downloadtp', $data);
	}

	public function get_tp($file)
	{
		$filePath = storage_path('app/public/tp/' . $file);

        return response()->download($filePath);
	}

	public function reset()
	{
		$nim = Auth::user()->nim;
		$modul = DB::select(DB::raw("SELECT max(id_modul) AS id FROM tp WHERE nim = " . $nim));
		$file = DB::select(DB::raw("SELECT file FROM tp WHERE nim = " . $nim . " AND id_modul = " . $modul[0]->id));

		$filePath = storage_path('app/public/tp/' . $file[0]->file);
		File::delete($filePath);

		$ret = DB::table('tp')->where([['nim', $nim], ['id_modul', $modul[0]->id]])->delete();

		return redirect()->route('input-tp');
	}
}
