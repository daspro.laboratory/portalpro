<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Alert;

class CheckNIMController extends Controller
{

	public function index(Request $request)
	{
		$nim = $request->nim;
		$result = DB::table('users')
		->where('nim', $nim)
		->get();

		$parse['status'] = "old";
		$parse['nama'] = explode(' ',trim($result[0]->nama));

		if ($result != '[]') {
			if ($result[0]->password == null) {
				$parse['status'] = "new";
				return $parse;
			}else{
				return $parse;
			}
		}else{
			return $parse;
		}
	}

	public function newpass(Request $request){
		$pass1 = $request->password;
		$pass2 = $request->password2;
		$nim = $request->nim;

		if ($pass1 == $pass2) {
			$result = DB::table('users')
			->where('nim', $nim)
			->update(['password' => Hash::make($pass1)]);
		}

		Alert::success('Terdaftar', 'Kamu berhasil mendaftar!');
		return redirect('/home');

	}
}
