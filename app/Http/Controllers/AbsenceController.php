<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

class AbsenceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_praktikum = Auth::user()->praktikum_aktif;
        $user_nim = Auth::user()->nim;

        $data['assistant'] = DB::select(
            DB::raw("SELECT id, nim, kode_asisten FROM detail_user_praktikum
                      WHERE id_praktikum = '". $user_praktikum ."'
                        AND role = 'ROLAS'")
        );

        $data['modul'] = DB::select(
            DB::raw("SELECT modul.id, modul.nama FROM modul
                      WHERE modul.id_praktikum = '". $user_praktikum ."'
                        AND NOT EXISTS (SELECT * FROM absensi
                                                WHERE absensi.modul = modul.id
                                                  AND absensi.nim = '". $user_nim ."')")
        );

        $data['history'] = DB::select(
            DB::raw("SELECT absensi.nim, absensi.modul, modul.nama,absensi.token, absensi.kode_asisten, absensi.status
                       FROM modul, absensi
                      WHERE absensi.nim = '". $user_nim ."'
                        AND absensi.modul = modul.id
                      ORDER BY absensi.modul ASC")
        );
        return view('absence', $data);
    }

    public function absence(Request $request)
    {
        Validator::make($request->all(), [
            'token' => 'required',
            'assistant' => 'required',
            'modul' => 'required'
        ])->validate();

        date_default_timezone_set('Asia/Jakarta');
        $token = strtoupper($request->token);
        $date = date("Y-m-d H:i:s");

        $data = DB::table('token')->select('token', 'expired_at')->where('token', $token)->get();

        if ($data->isEmpty()) {
            echo json_encode(['msg' => 'Invalid Token']);
        } else {
            if ($date > $data[0]->expired_at) {
              echo json_encode(['msg' => 'Token Expired']);
            } else {
                $params = [
                    'nim' => Auth::user()->nim,
                    'token' => $token,
                    'modul' => $request->modul,
                    'kode_asisten' => $request->assistant,
                    'status' => 'ABSIN',
                    'created_at' => $date,
                    'updated_at' => $date
                ];

                $save = DB::table('absensi')->insert($params);
                echo json_encode($save);
            }
        }
    }

    public function verification()
    {
        $user_code = Auth::user()->kode_asisten;

        $data['modul'] = DB::table('modul')
        ->select('id', 'nama')
        ->whereExists(function($query) {
            $query->select(DB::raw(1))
            ->from('absensi')
            ->whereRaw('absensi.modul = modul.id');
        })
        ->get();

        $i = 0;
        $data['absen_1'] = [];
        $data['absen'] = [];
        foreach ($data['modul'] as $row) {
            $data['absen'][$row->id] = DB::table('absensi')
            ->join('users', 'users.nim', '=', 'absensi.nim')
            ->select('absensi.nim', 'users.nama', 'users.kelas','absensi.token', 'absensi.modul', 'absensi.kode_asisten', 'absensi.status')
            ->where([['absensi.kode_asisten', $user_code], ['absensi.modul', $row->id]])
            ->orderBy('absensi.nim', 'asc')
            ->get();
        }

        if (!empty($data['absen'])) {
            $data['absen_1'] = $data['absen'][1];
        }

        $data['absen'] = json_encode($data['absen']);

        return view('verifikasiabsen', $data);
    }

    public function verify(Request $request)
    {
        $modul = $request->modul;
        $nim = $request->nim;

        $update = DB::table('absensi')
        ->where([['nim', $nim], ['modul', $modul]])
        ->update(['status' => 'ABSVR']);

        echo json_encode($update);
    }

}
