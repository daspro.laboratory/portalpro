<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Alert;

class GradeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        DB::enableQueryLog();
        $this->middleware('auth');
    }

    public function index()
    {
        $user_code = Auth::user()->kode_asisten;

        $data['modul'] = DB::select(
            DB::raw('SELECT modul.id, modul.id_praktikum, modul.nama FROM modul
              WHERE EXISTS (SELECT * FROM tp
              WHERE tp.id_modul = modul.id
              AND tp.kode_asisten = "'. $user_code .'")')
        );

        $data['nilai_1'] = [];
        $data['nilai'] = [];

        foreach ($data['modul'] as $row) {
            $data['nilai'][$row->id] = DB::select(
                DB::raw('SELECT absensi.nim, users.nama, users.kelas, (SELECT tes_awal FROM nilai WHERE nilai.nim = absensi.nim AND nilai.id_modul = absensi.modul) AS nilai_tp, (SELECT jurnal FROM nilai WHERE nilai.nim = absensi.nim AND nilai.id_modul = absensi.modul) AS nilai_jurnal, (SELECT tes_akhir FROM nilai WHERE nilai.nim = absensi.nim AND nilai.id_modul = absensi.modul) AS nilai_audit, (SELECT nilai_akhir FROM nilai WHERE nilai.nim = absensi.nim AND nilai.id_modul = absensi.modul) AS nilai_akhir
                 FROM absensi, users
                 WHERE absensi.nim = users.nim
                 AND absensi.kode_asisten = "'. $user_code .'"
                 AND absensi.modul = "'. $row->id .'"
                 ORDER BY users.kelas, users.nim')
            );
        }

        if (!empty($data['nilai'])) {
            $data['nilai_1'] = $data['nilai'][1];
        }

        $data['nilai'] = json_encode($data['nilai']);

    	return view('inputnilai', $data);
    }

    public function tp()
    {
        $user_code = Auth::user()->kode_asisten;

        $data['modul'] = DB::select(
            DB::raw('SELECT modul.id, modul.id_praktikum, modul.nama FROM modul
              WHERE EXISTS (SELECT * FROM tp
              WHERE tp.id_modul = modul.id
              AND tp.kode_asisten = "'. $user_code .'")')
        );

        $data['nilai_1'] = [];
        $data['nilai'] = [];

        foreach ($data['modul'] as $row) {
            $data['nilai'][$row->id] = DB::select(
                DB::raw('SELECT tp.nim, users.nama, users.kelas, (SELECT tes_awal FROM nilai WHERE nilai.nim = tp.nim AND nilai.id_modul = tp.id_modul) AS nilai
                 FROM tp, users
                 WHERE tp.nim = users.nim
                 AND tp.kode_asisten = "'. $user_code .'"
                 AND tp.id_modul = "'. $row->id .'"
                 ORDER BY users.kelas, users.nim')
            );
        }

        if (!empty($data['nilai'])) {
            $data['nilai_1'] = $data['nilai'][1];
        }

        $data['nilai'] = json_encode($data['nilai']);

        return view('inputnilaitp', $data);
    }

    public function input_tp(Request $request)
    {
        $user_code = Auth::user()->kode_asisten;
        $praktikum = $request->praktikum;
        $modul = $request->modul;
        $nim = $request->nim;
        $nilai = $request->nilai;

        date_default_timezone_set('Asia/Jakarta');
        $date = date("Y-m-d H:i:s");

        for ($i=0; $i < count($nim); $i++) {
            $grade = DB::select(
                DB::raw('SELECT tes_awal, jurnal, tes_akhir FROM nilai WHERE nim = "'. $nim[$i] .'" AND id_modul = '. $modul .' AND id_praktikum = ' . $praktikum)
            );

            $jurnal = (isset($grade[0]->jurnal)) ? $grade[0]->jurnal : null;
            $tes_akhir = (isset($grade[0]->tes_akhir)) ? $grade[0]->tes_akhir : null;

            $jurnal_calculate = (isset($grade[0]->jurnal)) ? $grade[0]->jurnal : 0;
            $tes_akhir_calculate = (isset($grade[0]->tes_akhir)) ? $grade[0]->tes_akhir : 0;

            $nilai_akhir = round(((0.2 * $nilai[$i]) + (0.6 * $jurnal_calculate) + (0.2 * $tes_akhir_calculate)), 1);

            $save = DB::table('nilai')
            ->updateOrInsert(
                [
                    'id_praktikum' => $praktikum,
                    'id_modul' => $modul,
                    'nim' => $nim[$i]
                ],
                [
                    'kode_asisten_tesawal' => $user_code,
                    'tes_awal' => round($nilai[$i], 1),
                    'nilai_akhir' => ($nilai[$i] == null) ? null : $nilai_akhir,
                    'created_at' => $date,
                    'updated_at' => $date
                ]
            );
        }

        return redirect()->route('input-nilai-tp');
    }

    public function input_nilai(Request $request)
    {
        $user_code = Auth::user()->kode_asisten;
        $praktikum = $request->praktikum;
        $modul = $request->modul;
        $nim = $request->nim;
        $jurnal = $request->jurnal;
        $audit = $request->audit;

        // dd($audit);

        date_default_timezone_set('Asia/Jakarta');
        $date = date("Y-m-d H:i:s");

        for ($i=0; $i < count($nim); $i++) {
            $grade = DB::select(
                DB::raw('SELECT tes_awal, jurnal, tes_akhir FROM nilai WHERE nim = "'. $nim[$i] .'" AND id_modul = '. $modul .' AND id_praktikum = ' . $praktikum)
            );

            $tes_awal = (isset($grade[0]->tes_awal)) ? $grade[0]->tes_awal : null;
            $tes_awal_calculate = (isset($grade[0]->tes_awal)) ? $grade[0]->tes_awal : 0;
            $nilai_akhir = round(((0.2 * $tes_awal_calculate) + (0.6 * $jurnal[$i]) + (0.2 * $audit[$i])), 1);

            $save = DB::table('nilai')
            ->updateOrInsert(
                [
                    'id_praktikum' => $praktikum,
                    'id_modul' => $modul,
                    'nim' => $nim[$i]
                ],
                [
                    'kode_asisten_praktikum' => $user_code,
                    'jurnal' => ($jurnal[$i] == null) ? null : (round($jurnal[$i], 1)),
                    'tes_akhir' => ($audit[$i] == null) ? null : (round($audit[$i], 1)),
                    'nilai_akhir' => ($audit[$i] == null && $jurnal[$i] == null) ? null : $nilai_akhir,
                    'created_at' => $date,
                    'updated_at' => $date
                ]
            );
        }

        return redirect()->route('inputnilai');
    }

}
