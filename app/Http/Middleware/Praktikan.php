<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Praktikan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = null;

        if(Auth::user() == null){
            return redirect('/login');
        }else{
            $role = Auth::user()->role;
        }

        if($role == 'Asistant'){
            return redirect('/home');
        }

        return $next($request);
    }
}
