@extends('layouts.app')

@section('title', 'Generate Token')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>Download File TP</h2>
			</div>
			<div class="modul">
				@foreach($modul as $data)
				<a class="btn btn-light modul-btn @if ($data->id == 1) active @endif" id="{{$data->id}}">Modul {{$data->id}}</a>
				@endforeach
			</div>
			
			<div class="inputtable-container">
				<table class="input-table">
					<thead class="bg-primary">
						<tr>
							<th scope="col" style="text-align: center;">NIM</th>
							<th scope="col" style="text-align: center;">NAMA</th>
							<th scope="col" style="text-align: center;">KELAS</th>
							<th scope="col" style="text-align: center;">TANGGAL</th>
							<th scope="col" style="text-align: center;">ACTION</th>
						</tr>
					</thead>
					<tbody>
						@foreach($tp_1 as $data)
						<tr>
							<td align="center">{{ $data->nim }}</td>
							<td align="center">{{ $data->nama }}</td>
							<td align="center">{{ $data->kelas }}</td>
							<td align="center">{{ $data->created_at }}</td>
							<td align="center">
								<a href="/get-tp/{{ $data->file }}" class="btn btn-warning download" download data-file="{{$data->file}}">Download</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
	var tp=JSON.parse('{!! $tp !!}');$('.modul-btn').click(function(){var thisId=$(this).attr('id');$('.input-table tbody tr').remove();$('.modul-btn').removeClass('active');$(this).addClass('active');var newRow='';$(tp[thisId]).each(function(i){newRow+='<tr>';newRow+='<td align="center">'+tp[thisId][i].nim+'</td>';newRow+='<td align="center">'+tp[thisId][i].nama+'</td>';newRow+='<td align="center">'+tp[thisId][i].kelas+'</td>';newRow+='<td align="center">'+tp[thisId][i].created_at+'</td>';newRow+='<td align="center"><a href="/get-tp/'+tp[thisId][i].file+'" class="btn btn-warning download" download>Download</a></td>';newRow+='</tr>'});$('.input-table tbody').append(newRow)});
</script>
@endsection