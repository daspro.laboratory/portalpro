@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>Beranda</h2>
			</div>
			<div class="banner">
				<h3>Praktikum Lebih Mudah dengan Portal Pro</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, neque non ipsa quis labore commodi beatae ipsam ducimus placeat iure distinctio quibusdam, ullam velit, eius itaque recusandae earum vel provident.</p>
			</div>
			<div class="referensi">
				<h3>Referensi dari Daspro Laboratory</h3>
				<div class="col-xl-6 col-xs-12 modul-ref">
					<h4>Modul</h4>
					<center>
						<iframe src="https://kegunung.com" frameborder="0"></iframe>
						<button class="btn btn-primary">Modul 1</button>
					</center>
				</div>
				<div class="col-xl-6 col-xs-12 video-ref">
					<h4>Video</h4>
				</div>
				<div class="see-more">
					<a href="" class="btn btn-link">See More</a>
				</div>
			</div>
			<hr>
			<div class="feedback">
				<h3>Feedback</h4>
					<h4 class="alay">Kolom Feedback</h4>
					<div class="kolom-feedback">
						<div class="input-feedback">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident aperiam ipsum doloribus alias sapiente, ipsa possimus assumenda laborum et ea fugiat esse autem nesciunt, quasi, corporis unde cumque expedita eos.</p>
							<form class="form-feedback">
								<input type="text" class="form-control" placeholder="Ketik pesan anda disini..">
								<button type="submit" class="btn btn-primary">Kirim</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection