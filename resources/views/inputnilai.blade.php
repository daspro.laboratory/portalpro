@extends('layouts.app')

@section('title', 'Generate Token')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>Input Nilai</h2>
			</div>
			<div class="modul">
				@foreach($modul as $data)
				<a class="btn btn-light modul-btn @if ($data->id == 1) active @endif" id="{{$data->id}}" data-praktikum="{{$data->id_praktikum}}">Modul {{$data->id}}</a>
				@endforeach
				<!-- <form action="" class="search">
						<input type="text" class="form-control search-input" placeholder="search">
				</form> -->
			</div>
			
			<form action="{{ route('submit-nilai') }}" method="POST">
				@csrf
				<input type="hidden" name="modul" id="modul">
				<input type="hidden" name="praktikum" id="praktikum">
				<div class="inputtable-container">
					<table class="input-table">
						<thead class="bg-primary">
							<tr>
								<th scope="col" style="text-align: center">NIM</th>
								<th scope="col" style="text-align: center">NAMA</th>
								<th scope="col" style="text-align: center">KELAS</th>
								<th scope="col" style="text-align: center" class="input">TES AWAL</th>
								<th scope="col" style="text-align: center" class="input">JURNAL</th>
								<th scope="col" style="text-align: center" class="input">AUDIT</th>
								<th scope="col" style="text-align: center" class="input">NILAI AKHIR</th>
							</tr>
						</thead>
						<tbody>
							@foreach($nilai_1 as $data)
							<tr>
								<td align="center"><input type="hidden" name="nim[]" value="{{ $data->nim }}">{{ $data->nim }}</td>
								<td align="center">{{ $data->nama }}</td>
								<td align="center">{{ $data->kelas }}</td>
								<td align="center">
									<input type="text" class="form-control form-nilai" name="tp[]" id="tp_{{$data->nim}}" value="{{ $data->nilai_tp }}" style="text-align: center" readonly>
								</td>
								<td align="center">
									<input type="text" class="form-control form-nilai" name="jurnal[]" id="jurnal_{{$data->nim}}"  value="{{ $data->nilai_jurnal }}" onchange="getGrade('{{$data->nim}}')" style="text-align: center">
								</td>
								<td align="center">
									<input type="text" class="form-control form-nilai" name="audit[]" id="audit_{{$data->nim}}"  value="{{ $data->nilai_audit }}" onchange="getGrade('{{$data->nim}}')" style="text-align: center">
								</td>
								<td align="center">
									<input type="text" class="form-control form-nilai" name="nilai_akhir[]" id="nilai_akhir_{{$data->nim}}"  value="{{ $data->nilai_akhir}}" style="text-align: center" readonly>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<button type="submit" class="btn btn-primary btn-save">
					Save
				</button>
			</form>
		</div>
	</div>
</div>
<script>
	var nilai=JSON.parse('{!! $nilai !!}');$('#modul').val($('.modul-btn.active').attr('id'));$('#praktikum').val($('.modul-btn.active').data('praktikum'));$('.modul-btn').click(function(){var thisId=$(this).attr('id');var thisPraktikum=$(this).data('praktikum');$('#modul').val(thisId);$('#praktikum').val(thisPraktikum);$('.input-table tbody tr').remove();$('.modul-btn').removeClass('active');$(this).addClass('active');var newRow='';$(nilai[thisId]).each(function(i){nilai[thisId][i].nilai_tp=(nilai[thisId][i].nilai_tp==null)?'':nilai[thisId][i].nilai_tp;nilai[thisId][i].nilai_jurnal=(nilai[thisId][i].nilai_jurnal==null)?'':nilai[thisId][i].nilai_jurnal;nilai[thisId][i].nilai_audit=(nilai[thisId][i].nilai_audit==null)?'':nilai[thisId][i].nilai_audit;nilai[thisId][i].nilai_akhir=(nilai[thisId][i].nilai_akhir==null)?'':nilai[thisId][i].nilai_akhir;newRow+='<tr>';newRow+='<td align="center"><input type="hidden" name="nim[]" value="'+nilai[thisId][i].nim+'">'+nilai[thisId][i].nim+'</td>';newRow+='<td align="center">'+nilai[thisId][i].nama+'</td>';newRow+='<td align="center">'+nilai[thisId][i].kelas+'</td>';newRow+='<td align="center"><input type="text" class="form-control form-nilai" id="tp_'+nilai[thisId][i].nim+'" name="tp[]" value="'+nilai[thisId][i].nilai_tp+'" style="text-align: center" readonly></td>';newRow+='<td align="center"><input type="text" class="form-control form-nilai" id="jurnal_'+nilai[thisId][i].nim+'" name="jurnal[]" value="'+nilai[thisId][i].nilai_jurnal+'" onchange="getGrade('+nilai[thisId][i].nim+')" style="text-align: center"></td>';newRow+='<td align="center"><input type="text" class="form-control form-nilai" id="audit_'+nilai[thisId][i].nim+'" name="audit[]" value="'+nilai[thisId][i].nilai_audit+'" onchange="getGrade('+nilai[thisId][i].nim+')" style="text-align: center"></td>';newRow+='<td align="center"><input type="text" class="form-control form-nilai" id="nilai_akhir_'+nilai[thisId][i].nim+'" name="nilai_akhir[]" value="'+nilai[thisId][i].nilai_akhir+'" style="text-align: center" readonly></td>';newRow+='</tr>'});$('.input-table tbody').append(newRow)});function getGrade(nim){tp=($('#tp_'+nim).val()==null)?0:$('#tp_'+nim).val();jurnal=($('#jurnal_'+nim).val()==null)?0:$('#jurnal_'+nim).val();audit=($('#audit_'+nim).val()==null)?0:$('#audit_'+nim).val();var grade=(0.2*tp)+(0.6*jurnal)+(0.2*audit);$('#nilai_akhir_'+nim).val(grade.toFixed(1))}
</script>
@endsection