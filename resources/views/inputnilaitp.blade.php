@extends('layouts.app')

@section('title', 'Generate Token')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>Input Nilai TP</h2>
			</div>
			<div class="modul">
				@foreach($modul as $data)
				<a class="btn btn-light modul-btn @if ($data->id == 1) active @endif" id="{{$data->id}}" data-praktikum="{{$data->id_praktikum}}">Modul {{$data->id}}</a>
				@endforeach
				<!-- <form action="" class="search">
					<input type="text" class="form-control search-input" placeholder="search">
				</form> -->
			</div>
			
			<form action="{{ route('submit-nilai-tp') }}" method="POST">
				@csrf
				<input type="hidden" name="modul" id="modul">
				<input type="hidden" name="praktikum" id="praktikum">
				<div class="inputtable-container">
					<table class="input-table">
						<thead class="bg-primary">
							<tr>
								<th scope="col" style="text-align: center">NIM</th>
								<th scope="col" style="text-align: center">NAMA</th>
								<th scope="col" style="text-align: center">KELAS</th>
								<th scope="col" style="text-align: center" class="input">NILAI TP</th>
							</tr>
						</thead>
						<tbody>
							@foreach($nilai_1 as $data)
							<tr>
								<td align="center"><input type="hidden" name="nim[]" value="{{ $data->nim }}">{{ $data->nim }}</td>
								<td align="center">{{ $data->nama }}</td>
								<td align="center">{{ $data->kelas }}</td>
								<td align="center"><input type="text" class="form-control form-nilai" name="nilai[]" value="{{ $data->nilai }}" style="text-align: center"></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<button type="submit" class="btn btn-primary btn-save">
					SUBMIT
				</button>
			</form>
		</div>
	</div>
</div>
<script>
	var nilai=JSON.parse('{!! $nilai !!}');$('#modul').val($('.modul-btn.active').attr('id'));$('#praktikum').val($('.modul-btn.active').data('praktikum'));$('.modul-btn').click(function(){var thisId=$(this).attr('id');var thisPraktikum=$(this).data('praktikum');$('#modul').val(thisId);$('#praktikum').val(thisPraktikum);$('.input-table tbody tr').remove();$('.modul-btn').removeClass('active');$(this).addClass('active');var newRow='';$(nilai[thisId]).each(function(i){nilai[thisId][i].nilai=(nilai[thisId][i].nilai==null)?'':nilai[thisId][i].nilai;newRow+='<tr>';newRow+='<td align="center"><input type="hidden" name="nim[]" value="'+nilai[thisId][i].nim+'">'+nilai[thisId][i].nim+'</td>';newRow+='<td align="center">'+nilai[thisId][i].nama+'</td>';newRow+='<td align="center">'+nilai[thisId][i].kelas+'</td>';newRow+='<td align="center"><input type="text" class="form-control form-nilai" name="nilai[]" value="'+nilai[thisId][i].nilai+'" style="text-align: center"></td>';newRow+='</tr>'});$('.input-table tbody').append(newRow)});
</script>
@endsection