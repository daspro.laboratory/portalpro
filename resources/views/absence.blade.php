@extends('layouts.app')

@section('title', 'Absence')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>Absen</h2>
			</div>
			<div class="inputtoken-container">
				<div class="already-absent" style="display: none;">
					<h4>Kamu Sudah Absen Minggu Ini</h4>
				</div>
				<div class="form-absence">
					<h4>Input Token Absen</h4>
					<form class="form-feedback">
						<input type="text" class="form-control" style="text-align: center; font-size: 30px; margin: 70px 10px 10px 10px;" id="token" name="token">
						<span class="invalid-feedback msg-token" role="alert">
							<strong>test</strong>
						</span>
						<div class="form-group absense-option">
							<select class="form-control" id="assistant" name="assistant" style="margin-top: 50px;">
								<option value="">Pilih Asisten</option>
								@foreach($assistant as $data)
								<option value="{{$data->kode_asisten}}">{{$data->kode_asisten}}</option>
								@endforeach
							</select>
							<span class="invalid-feedback msg-assistant" role="alert">
								<strong>test</strong>
							</span>
						</div>
						<div class="form-group absense-option">
							<select class="form-control" id="modul" name="modul">
								<option value="">Pilih Modul</option>
								@foreach($modul as $data)
								<option value="{{$data->id}}">Modul {{$data->id}} - {{$data->nama}}</option>
								@endforeach
							</select>
							<span class="invalid-feedback msg-modul" role="alert">
								<strong>test</strong>
							</span>
						</div>
						<button class="btn btn-primary btn-token" type="button" id="absence">Absen</button>
					</form>
				</div>
			</div>
			<div class="inputtable-container">
				<table class="input-table history-absence">
					<thead class="bg-primary">
						<tr>
							<th scope="col" align="center">No</th>
							<th scope="col" align="center">Token</th>
							<th scope="col" align="center">Modul</th>
							<th scope="col" align="center">Kode Asisten</th>
							<th scope="col" align="center">Status</th>
						</tr>
					</thead>
					<tbody>
						@if(count($history))
						@for($i = 0; $i < count($history); $i++)
						<tr>
							<td align="center">{{ $i + 1 }}</td>
							<td align="center">{{ $history[$i]->token }}</td>
							<td align="center">Modul {{ $history[$i]->modul }} - {{ $history[$i]->nama }}</td>
							<td align="center">{{ $history[$i]->kode_asisten }}</td>
							<td align="center">@if($history[$i]->status == 'ABSIN') Terinput @else Terverifikasi @endif</td>
						</tr>
						@endfor
						@else
						<tr class="nodata">
							<td align="center" colspan="5">No Data</td>
						</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){if($('#modul option').length==1){$('.already-absent').removeAttr('style');$('.form-absence').css('display','none')}$('#absence').click(function(){$('span[class*="msg-"').each(function(){$(this).css('display','none')});var token=$('#token').val();var assistant=$('#assistant').val();var modul=$('#modul').val();$.ajax({type:'POST',url:'/absence',data:{token:token,assistant:assistant,modul:modul},dataType:'json',headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},success:function(response){if(typeof(response.msg)!='undefined'){var errors=response.msg;$('.msg-token').html(errors).css('display','block');return false}$('.nodata').remove();var no=$('table.history-absence tbody tr').length+1;var newRow;newRow+='<tr>';newRow+='<td align="center">'+no+'</td>';newRow+='<td align="center">'+token+'</td>';newRow+='<td align="center">'+$('#modul option:selected').html()+'</td>';newRow+='<td align="center">'+assistant+'</td>';newRow+='<td align="center">Terinput</td>';newRow+='</tr>';$('table.history-absence tbody').append(newRow);$('.already-absent').removeAttr('style');$('.form-absence').css('display','none')},error:function(response){if(response.status==422){var errors=response.responseJSON.errors;$.each(errors,function(key){$('.msg-'+key).html($(this)[0]).css('display','block')})}}})})});
</script>
@endsection
