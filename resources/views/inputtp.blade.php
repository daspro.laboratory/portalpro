@extends('layouts.app')

@section('title', 'Upload TP')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>Upload TP</h2>
			</div>
			<div class="inputtoken-container">
				<div class="already-uploaded" style="display: none;">
					<h4>Kamu Sudah Upload TP Minggu Ini</h4>
					<button class="btn btn-primary btn-token" type="button" id="reset">Upload Ulang</button>
				</div>
				<div class="form-tp">
					<h4 style="margin-bottom: 60px;">Upload Tugas Pendahuluan</h4>
					<form class="form-feedback">
						<div class="form-group absense-option" style="text-align: left; margin-bottom: -45px; position: relative;">
							<div class="custom-file">
								<input type="file" class="custom-file-input form-control" id="file" name="file" required>
								<label class="custom-file-label" for="file">Pilih File...</label>
							</div>
							<span class="invalid-feedback msg-file" role="alert" style="margin-top: -15px; padding-bottom: 15px; text-align: center;">
								<strong>test</strong>
							</span>
						</div>
						<div class="form-group absense-option">
							<select class="form-control" id="assistant" name="assistant" style="margin-top: 50px;">
								<option value="">Pilih Asisten</option>
								@foreach($assistant as $data)
								<option value="{{$data->kode_asisten}}">{{$data->kode_asisten}}</option>
								@endforeach
							</select>
							<span class="invalid-feedback msg-assistant" role="alert">
								<strong>test</strong>
							</span>
						</div>
						<div class="form-group absense-option">
							<select class="form-control" id="modul" name="modul">
								<option value="">Pilih Modul</option>
								@foreach($modul as $data)
								<option value="{{$data->id}}">Modul {{$data->id}} - {{$data->nama}}</option>
								@endforeach
							</select>
							<span class="invalid-feedback msg-modul" role="alert">
								<strong>test</strong>
							</span>
						</div>
						<button class="btn btn-primary btn-token" type="button" id="absence">Upload TP</button>
					</form>
				</div>
			</div>
			<div class="inputtable-container">
				<table class="input-table history-tp">
					<thead class="bg-primary">
						<tr>
							<th scope="col" style="text-align: center;">No</th>
							<th scope="col" style="text-align: center;">Modul</th>
							<th scope="col" style="text-align: center;">Kode Asisten</th>
							<th scope="col" style="text-align: center;">File</th>
						</tr>
					</thead>
					<tbody>
						@if(count($history))
						@for($i = 0; $i < count($history); $i++)
						<tr>
							<td align="center">{{ $i + 1 }}</td>
							<td align="center">Modul {{ $history[$i]->id_modul }}</td>
							<td align="center">{{ $history[$i]->kode_asisten }}</td>
							<td align="center">{{ $history[$i]->file }}</td>
						</tr>
						@endfor
						@else
						<tr class="nodata">
							<td align="center" colspan="5">No Data</td>
						</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
	$('#file').change(function(){var file=$(this).val().substr($(this).val().lastIndexOf('\\')+1);$('.custom-file-label').html(file.substr(0,10))});if($('#modul option').length==1){$('.already-uploaded').removeAttr('style');$('.form-tp').css('display','none')}$('#absence').click(function(){$('span[class*="msg-"').each(function(){$(this).css('display','none')});var formData=new FormData($('.form-feedback')[0]);var fileName=$('#file').val().replace(/^.*[\\\/]/,'');$.ajax({type:'POST',url:'/upload-tp',data:formData,processData:false,contentType:false,cache:false,dataType:'json',enctype:'multipart/form-data',headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},success:function(response){$('.nodata').remove();var no=$('table.history-tp tbody tr').length+1;var newRow;newRow+='<tr>';newRow+='<td align="center">'+no+'</td>';newRow+='<td align="center">'+$('#modul option:selected').html()+'</td>';newRow+='<td align="center">'+$('#assistant option:selected').val()+'</td>';newRow+='<td align="center">'+fileName+'</td>';newRow+='</tr>';$('table.history-tp tbody').append(newRow);$('.already-uploaded').removeAttr('style');$('.form-tp').css('display','none')},error:function(response){if(response.status==422){var errors=response.responseJSON.errors;$.each(errors,function(key){$('.msg-'+key).html($(this)[0]).css('display','block')})}}})});

	$('#reset').click(function() {
		window.location.href = '/reset-tp';
	});
</script>
@endsection