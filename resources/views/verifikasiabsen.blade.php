@extends('layouts.app')

@section('title', 'Verifikasi Absen')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>Verifikasi Absen</h2>
			</div>
			<div class="modul">
				@foreach($modul as $data)
				<a class="btn btn-light modul-btn @if ($data->id == 1) active @endif" id="{{$data->id}}">Modul {{$data->id}}</a>
				@endforeach
			</div>

			<form action="">
				<div class="inputtable-container">
					<table class="input-table" style="text-align:center;">
						<thead class="bg-primary">
							<tr>
								<th scope="col">NIM</th>
								<th scope="col">NAMA</th>
								<th scope="col">KELAS</th>
								<th scope="col">TOKEN</th>
								<th scope="col">ACTION</th>
							</tr>
						</thead>
						<tbody>
							@foreach($absen_1 as $data)
							<tr>
								<td>{{ $data->nim }}</td>
								<td>{{ $data->nama }}</td>
								<td>{{ $data->kelas }}</td>
								<td>{{ $data->token }}</td>
								@if($data->status == 'ABSIN')
								<td><button type="button" class="btn btn-warning verify" data-nim="{{$data->nim}}" data-modul="{{$data->modul}}">Verifikasi</button></td>
								@else
								<td>Terverifikasi</td>
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	var absen=JSON.parse('{!! $absen !!}');$('.modul-btn').click(function(){var thisId=$(this).attr('id');$('.input-table tbody tr').remove();$('.modul-btn').removeClass('active');$(this).addClass('active');var newRow='';$(absen[thisId]).each(function(i){newRow+='<tr>';newRow+='<td>'+absen[thisId][i].nim+'</td>';newRow+='<td>'+absen[thisId][i].nama+'</td>';newRow+='<td>'+absen[thisId][i].kelas+'</td>';newRow+='<td>'+absen[thisId][i].token+'</td>';if(absen[thisId][i].status=='ABSIN'){newRow+='<td><button type="button" class="btn btn-warning verify" data-nim="'+absen[thisId][i].nim+'" data-modul="'+absen[thisId][i].modul+'">Verifikasi</button></td>'}else{newRow+='<td>Terverifikasi</td>'}newRow+='</tr>'});$('.input-table tbody').append(newRow)});$(document).on('click','button.verify',function(){var $this=$(this);var nim=$this.data('nim');var modul=$this.data('modul');$.ajax({type:'POST',url:'/verify',data:{nim:nim,modul:modul},dataType:'json',headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},success:function(response){if(response){$this.parent().html('Terverifikasi');var index=absen[modul].findIndex(p=>p.nim==nim);absen[modul][index].status='ABSVR'}}})})
</script>
@endsection