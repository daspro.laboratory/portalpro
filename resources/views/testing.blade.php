@extends('layouts.app')

@section('title', 'Absence')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>Absen</h2>
			</div>
			<div class="inputtoken-container">
				<div class="already-absent" style="display: none;">
					<h4>Kamu Sudah Absen Minggu Ini</h4>
				</div>
				<div class="form-absence">
					<h4>Input Token Absen</h4>
					<form class="form-feedback">
						<input type="text" class="form-control" style="text-align: center; font-size: 30px; margin: 70px 10px 10px 10px;" id="token" name="token">
						<span class="invalid-feedback msg-token" role="alert">
							<strong>test</strong>
						</span>
						<div class="form-group absense-option">
							<select class="form-control" id="assistant" name="assistant" style="margin-top: 50px;">
								<option value="">Pilih Asisten</option>
								@foreach($assistant as $data)
								<option value="{{$data->kode_asisten}}">{{$data->kode_asisten}}</option>
								@endforeach
							</select>
							<span class="invalid-feedback msg-assistant" role="alert">
								<strong>test</strong>
							</span>
						</div>
						<div class="form-group absense-option">
							<select class="form-control" id="modul" name="modul">
								<option value="">Pilih Modul</option>
								@foreach($modul as $data)
								<option value="{{$data->id}}">Modul {{$data->id}} - {{$data->nama}}</option>
								@endforeach
							</select>
							<span class="invalid-feedback msg-modul" role="alert">
								<strong>test</strong>
							</span>
						</div>
						<button class="btn btn-primary btn-token" type="button" id="absence">Absen</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
