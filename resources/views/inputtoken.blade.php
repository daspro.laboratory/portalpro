<?php include 'partial/header.php'; ?>

<?php include 'partial/navbar.php'; ?>

<?php include 'partial/sidebar.php'; ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h2>Tes Awal / Jurnal</h2>
			</div>
			<div class="inputtoken-container">
				<h4>Input Token</h4>
				<form class="form-feedback">
					<input type="text" class="form-control" style="text-align: center; font-size: 30px; margin: 70px 10px;">
					<button class="btn btn-primary btn-token" type="submit">Lanjut</button>
				</form>
			</div>
		</div>
	</div>
</div>

<?php include 'partial/footer.php'; ?>