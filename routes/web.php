<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//asistant middleware
Route::middleware('verifikasiasistant')->group(function() {

	Route::get('/dashboardasistant', 'TokenController@index');

	Route::get('/token', 'TokenController@generate');

	Route::get('/inputnilai', 'GradeController@index')->name('inputnilai');
	Route::post('/submit-nilai', 'GradeController@input_nilai')->name('submit-nilai');

	Route::get('/verifikasiabsen', 'AbsenceController@verification');
	Route::post('/verify', 'AbsenceController@verify')->name('verify');

	Route::get('/download-tp', 'InputTPController@download')->name('download-tp');
	Route::get('/get-tp/{file}', 'InputTPController@get_tp')->name('get-tp');

	Route::get('/input-nilai-tp', 'GradeController@tp')->name('input-nilai-tp');
	Route::post('/submit-nilai-tp', 'GradeController@input_tp')->name('submit-nilai-tp');

	Route::get('/benerinakun', 'EditProfileController@index');

});

//praktikan middleware
Route::middleware('verifikasipraktikan')->group(function() {
	Route::get('/dashboardpraktikan', 'AbsenceController@index')->name('dashboardpraktikan');
	Route::post('/absence', 'AbsenceController@absence')->name('absence');
	Route::get('/input-tp', 'InputTPController@index')->name('input-tp');
	Route::post('/upload-tp', 'InputTPController@upload')->name('upload-tp');
	Route::get('/reset-tp', 'InputTPController@reset');
	Route::get('/nilai', 'NilaiController@index');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomeController@index')->name('home');

Route::post('/check', 'CheckNIMController@index');

Route::post('/newpass', 'CheckNIMController@newpass');
